﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatronesConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(Singleton.Instance.mensaje);
            Singleton.Instance.mensaje = "El mensaje ya cambio";
            Console.WriteLine(Singleton.Instance.mensaje);

            Console.ReadLine();
        }
    }
}
