﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototypeConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Animal oAnimal = new Animal() { Nombre = "Oveja Dolly", Patas=4 };
            oAnimal.Rasgos = new Detalles();
            oAnimal.Rasgos.Color = "Blanca";
            oAnimal.Rasgos.Raza = "Oveja Blanca";

            Animal oAnimalClonado = oAnimal.Clone() as Animal;

            oAnimalClonado.Rasgos.Color = "Negro";
            oAnimalClonado.Nombre = "Oveja Negra";
            //oAnimal.Patas = 5;
            //Console.WriteLine(oAnimal.Patas);
            //Console.WriteLine(oAnimalClonado.Patas);

            Console.WriteLine("Color animal original: " + oAnimal.Rasgos.Color);
            Console.WriteLine("Color animal clonado: " + oAnimalClonado.Rasgos.Color);
            Console.WriteLine("Nombre animal original: " + oAnimal.Nombre);
            Console.WriteLine("Nombre animal original: " + oAnimalClonado.Nombre);

            Console.ReadLine();
        }
    }
}
